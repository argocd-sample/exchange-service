FROM openjdk:17-alpine
MAINTAINER melihatalayy@gmail.com
VOLUME /tmp
COPY ./target/exchange-service-*.jar exchange-service.jar
EXPOSE 8000
ENTRYPOINT ["java","-jar","/exchange-service.jar"]