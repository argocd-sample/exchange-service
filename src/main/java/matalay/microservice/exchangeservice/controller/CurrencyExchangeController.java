package matalay.microservice.exchangeservice.controller;


import matalay.microservice.exchangeservice.model.CurrencyExchange;
import matalay.microservice.exchangeservice.service.CurrencyExchangeService;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CurrencyExchangeController {

    private final Environment environment;
    private final CurrencyExchangeService exchangeService;

    public CurrencyExchangeController(Environment environment, CurrencyExchangeService exchangeService) {
        this.environment = environment;
        this.exchangeService = exchangeService;
    }

    @GetMapping("/from/{from}/to/{to}")
    public CurrencyExchange retrieveExchangeValue(@PathVariable String from, @PathVariable String to){
        String port = environment.getProperty("local.server.port");
        CurrencyExchange currencyExchange = exchangeService.getCurrencyExchangeByFromAndTo(from,to);
        currencyExchange.setEnvironment(port);
        return currencyExchange;
    }
}
