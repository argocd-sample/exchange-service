package matalay.microservice.exchangeservice.controller;

//import io.github.resilience4j.retry.annotation.Retry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class CircuitBreakerController {

    private final Logger logger = LoggerFactory.getLogger(CircuitBreakerController.class);
    private static int tryCount = 0;
    @GetMapping("/sample-api")
    //@Retry(name = "sample-api", fallbackMethod = "sampleApiFallBack")
    public String getSampleApi(){
        logger.info("Sample Api Try : {}", ++tryCount);
        ResponseEntity<String> result = new RestTemplate().getForEntity("http://localhost:9090/sample-api", String.class);
        return result.getBody();
    }

    public String sampleApiFallBack(RuntimeException exception){
        return "Sample api did not give feed back";
    }
}
