package matalay.microservice.exchangeservice.repository;


import matalay.microservice.exchangeservice.model.CurrencyExchange;
import org.springframework.data.jpa.repository.JpaRepository;
public interface CurrencyExchangeRepository extends JpaRepository<CurrencyExchange,Integer> {

    CurrencyExchange findByFromAndTo(String from, String to);
}
