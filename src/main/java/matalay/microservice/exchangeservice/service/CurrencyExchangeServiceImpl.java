package matalay.microservice.exchangeservice.service;


import matalay.microservice.exchangeservice.model.CurrencyExchange;
import matalay.microservice.exchangeservice.repository.CurrencyExchangeRepository;
import org.springframework.stereotype.Service;

@Service
public class CurrencyExchangeServiceImpl implements CurrencyExchangeService{

    private final CurrencyExchangeRepository exchangeRepository;
    public CurrencyExchangeServiceImpl(CurrencyExchangeRepository exchangeRepository) {
        this.exchangeRepository = exchangeRepository;
    }
    @Override
    public CurrencyExchange getCurrencyExchangeByFromAndTo(String from, String to) {
        return exchangeRepository.findByFromAndAndTo(from,to);
    }
}
