package matalay.microservice.exchangeservice.service;


import matalay.microservice.exchangeservice.model.CurrencyExchange;

public interface CurrencyExchangeService {

    CurrencyExchange getCurrencyExchangeByFromAndTo(String from, String to);
}
